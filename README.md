# Partition

Ce programme consiste à générer une partition d'une liste en sous listes. Le framework utlisé est Springboot

On va utiliser JAVA 8 pour partionner les listes. Plusieurs techniques sont utilisées:

* Les Collectors partitioningBy
* Les Collectors groupingBy
* Split the List by separator
